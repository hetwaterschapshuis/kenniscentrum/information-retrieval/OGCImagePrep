import re
import numpy as np

from collections import namedtuple
from owslib.wmts import WebMapTileService
from rasterio.io import MemoryFile

XYTuple = namedtuple("XY_tuple", ["x", "y"])


class WMTSConnector:
    """Class which allows you to connect with a Web Map Tile Service (WMTS) and download tiles.
    Can be used to
        1. Download a tile at a zoomlevel containing the given coordinates.
        2. Download an image with a given radius around the given coordinates.

    Parameters
    ----------
    wmts_link : str
        Link to WMTS.
    layer : str
        Name of the WMTS layer to retrieve images from.
    tile_matrix_set : str
        Name of the tile matrix set to use.
    top_left_corner : dict
        Dict containing x and y keys with values the float representing the coordinates of the top.
        left corner of the WMTS.
    dpi : float
        Dots per meter of the TileMatrix.
    """
    def __init__(self, wmts_link, layer, tile_matrix_set, zoom_level, tile_format="image/jpg",
                 dpi=0.28e-3):
        self.wmts = WebMapTileService(wmts_link)
        self.layer = layer
        self.tile_matrix_set = tile_matrix_set
        self.tile_format = tile_format
        self.dpi = dpi
        self.set_zoom_level(zoom_level)

    def get_image_around_coords(self, x, y, width_x, length_y):
        """Retrieve an image of certain width and heigth around the given coordinates.

        Parameters
        ----------
        x : float
            X-coordinate, same coordinate system as the WMTS.
        y : float
            Y-coordinate, same coordinate system as the WMTS.
        width_x : float
            Meters that the image must represent on the x-axis.
        length_y : [type]
            Meters that the image must represent on the y-axis.

        Returns
        -------
        img: np.array
            Image array with dimensions [pixels_y, pixels_x, 3].
        """
        min_row, max_row, min_col, max_col = self._get_minmax_rowcol(x, y, width_x, length_y)
        img = self._bundle_all_tiles(min_row, max_row, min_col, max_col)
        img = self._cut_image(img, min_row, max_row, min_col, max_col)
        return img

    def get_tile_at_coords(self, x, y):
        """Retrieve the tile at the coordinates.

        Parameters
        ----------
        x : float
            X-coordinate, same coordinate system as the WMTS.
        y : float
            Y-coordinate, same coordinate system as the WMTS.

        Returns
        -------
        img : np.array (of: Numpy Array)
            Image array with dimensions [number_of_pixels, number_of_pixels, 3].
        """

        row, column = self._get_rowcol_at_coords(x, y)
        img = self.get_tile_at_rowcol(row, column)

        return img

    def get_tile_at_coords_with_dot(self, x, y, color_hex="#FF0000"):
        """Retrieves tile at the coordinations and adds a colored dot at the given coordinates.

        Parameters
        ----------
        x : float
            X-coordinate, same coordinate system as the WMTS.
        y : float
            Y-coordinate, same coordinate system as the WMTS.
        color_hex: str, optional
            Three- or six-character hex code for the marker color.
            The default is '#FF0000' (red).

        Returns
        -------
        img : np.array (of: Numpy Array)
            Image array with dimensions [number_of_pixels, number_of_pixels, 3].
        """

        img = self.get_tile_at_coords(x, y)
        row, column = self._get_rowcol_at_coords(x, y)

        img = self._add_dot_to_image(img, row, column, color_hex)

        return img

    def _add_dot_to_image(self, img, row, column, color_hex):
        """Adds a dot at the specified location.

        Parameters
        ----------
        img : np.array
            Image array with dimensions [number_of_pixels, number_of_pixels, 3].
        row : float
            Row number of WMTS of location of dot.
        column : float
            Column number of WMTS of location of dot.
        color_hex: str
            Three- or six-character hex code for the dot color.

        Returns
        -------
        img : np.array
            Image array (dotted) with dimensions [number_of_pixels, number_of_pixels, 3].
        """
        row_remainder = row % 1
        col_remainder = column % 1

        row_pixel = int(row_remainder * img.shape[0])
        col_pixel = int(col_remainder * img.shape[1])

        # Make sure we don't try to dot outside of the array
        max_row = img.shape[0] - 1
        max_column = img.shape[1] - 1

        # color pixels including to and adjacent to the point
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                row_pixel_i = min(max(row_pixel + i, 0), max_row)
                col_pixel_j = min(max(col_pixel + j, 0), max_column)
                dot_rgb = hex_to_rgb(color_hex)
                img[row_pixel_i, col_pixel_j] = dot_rgb

        return img

    def set_zoom_level(self, zoom_level):
        """Set zoom level from which to extract tiles.

        Parameters
        ----------
        zoom_level : int
            Zoom level of tile matrix set
        """
        self.zoom_level = zoom_level
        tilematrix = self.wmts.tilematrixsets[self.tile_matrix_set].tilematrix[str(zoom_level)]
        self.tlc = XYTuple(x=tilematrix.topleftcorner[0], y=tilematrix.topleftcorner[1])
        self.scale_denominator = tilematrix.scaledenominator
        self.nr_pixels = XYTuple(x=tilematrix.tilewidth, y=tilematrix.tileheight)

    def _bundle_all_tiles(self, min_row, max_row, min_col, max_col):
        """Retrieves all tiles of the specified rows and columns from the wmts and bundles them
        together in one big image (numpy array) and returns this.

        Parameters
        ----------
        min_row : int or float
            The row tile number of the first part of the image needed.
        max_row : int or float
            The row tile number of the last part of the image needed.
        min_col : int or float
            The column tile number of the first part of the image needed.
        max_col : int or float
            The column tile number of the last part of the image needed.

        Returns
        -------
        img: np.array
            Image array with dimensions [pixels_y, pixels_x, 3].
        """
        nr_pixels = self.nr_pixels

        nr_rows = int(max_row) - int(min_row) + 1
        nr_cols = int(max_col) - int(min_col) + 1

        img = np.empty([nr_pixels.y*nr_rows, nr_pixels.x*nr_cols, 3], dtype=int)

        for i in range(nr_rows):
            row_new_img = min_row + i
            for j in range(nr_cols):
                col_new_img = min_col + j

                new_img = self.get_tile_at_rowcol(row_new_img, col_new_img)
                image_x_min = nr_pixels.y*i
                image_x_max = nr_pixels.y*(i+1)
                image_y_min = nr_pixels.x*j
                image_y_max = nr_pixels.x*(j+1)
                img[image_x_min:image_x_max, image_y_min:image_y_max] = new_img

        return img

    def _cut_image(self, img, min_row, max_row, min_col, max_col):
        """Cut the image to only keep the part of the image that is between min_col and max_col
        and between min_row and max_row.

        First we load in all tiles and bundle them into one big image. This big image will have a
        bigger height and width than desired. This function cuts out the part of the image than we
        desire.

        Parameters
        ----------
        img : Numpy Array
            Image in Array-form of dimensions [pixels_y, pixels_x, 3].
        min_row : float
            The row tile number of the first part of the image needed.
            Unrounded, e.g. 37763.9.
        max_row : float
            The row tile number of the last part of the image needed.
            Unrounded, e.g. 37766.9.
        min_col : float
            The column tile number of the first part of the image needed.
            Unrounded, e.g. 28680.3.
        max_col : float
            The column tile number of the last part of the image needed.
            Unrounded, e.g. 28683.3

        Returns
        -------
        img: np.array
            Image array with dimensions [pixels_y, pixels_x, 3].
        """
        nr_pixels_x = round((max_col-min_col) * self.nr_pixels.x)
        first_x = round(min_col % 1 * self.nr_pixels.x)
        last_x = first_x + nr_pixels_x

        nr_pixels_y = round((max_row-min_row) * self.nr_pixels.y)
        first_y = round(min_row % 1 * self.nr_pixels.y)
        last_y = first_y + nr_pixels_y

        img = img[first_y:last_y, first_x:last_x]
        return img

    def _get_minmax_rowcol(self, x, y, width_x, length_y):
        """Retrieves the min and max row and columns of the tiles needed to be obtained in order to
        build a image of coordinates x and y with the specified width for x and width for y.

        Parameters
        ----------
        x : float
            X-coordinate, same coordinate system as the WMTS.
        y : float
            Y-coordinate, same coordinate system as the WMTS.
        width_x : float
            Meters that the image must represent on the x-axis.
        length_y : [type]
            Meters that the image must represent on the y-axis.

        Returns
        -------
        tuple of 4 floats
            Float representing the min and max row and cols of the tiles we need to create the new
            image. In the shape (min_row, max_row, min_col, max_col)
        """

        x_min = x - width_x / 2
        y_min = y - length_y / 2
        x_max = x + width_x / 2
        y_max = y + length_y / 2

        max_row, min_col = self._get_rowcol_at_coords(x_min, y_min)
        min_row, max_col = self._get_rowcol_at_coords(x_max, y_max)

        return min_row, max_row, min_col, max_col

    def get_tile_at_rowcol(self, row, column):
        """Retrieve the tile at the specified row and column

        Parameters
        ----------
        row : float or int
            Row from tile to obtain.
        column : float or int
            Column from tile to obtain.

        Returns
        -------
        img: np.array
            Image array in Array-form with dimensions [number_of_pixels, number_of_pixels, 3].
        """
        rw = self.wmts.gettile(layer=self.layer,
                               tileMatrixSet=self.tile_matrix_set,
                               tilematrix=str(self.zoom_level),
                               row=int(row),
                               column=int(column),
                               format=self.tile_format)

        with MemoryFile(rw.read()) as memfile:
            with memfile.open() as dataset:
                img = dataset.read()

        img = np.transpose(img, (1, 2, 0))
        return img

    def _get_rowcol_at_coords(self, x, y, as_int=False):
        """Get the row and column of the tile from the WMTS where the coordinates x and
        y are situated on.

        Parameters
        ----------
        x : float
            x-coordinate, same coordinate system as the WMTS
        y : float
            y-coordinate, same coordinate system as the WMTS
        as_int : bool
            Whether to return the column and row as an int. By default false, indicating that
            the return values are floats.

        Returns
        -------
        tuple of float or int
            The row and column of the tile where the coordinates are situated on.
        """
        size_tile = self._get_size_tile()
        row = (self.tlc.y - y) / size_tile.y
        column = (x - self.tlc.x) / size_tile.x

        if as_int:
            row, column = int(row), int(column)

        return row, column

    def _get_size_tile(self):
        """Get the size the tile represents of the current zoom level in meters

        Parameters
        ----------

        Returns
        -------
        size_tile: XYTuple
            The meters the tile represents.
        """
        size_tile_x = self.dpi * self.nr_pixels.x * self.scale_denominator
        size_tile_y = self.dpi * self.nr_pixels.y * self.scale_denominator
        size_tile = XYTuple(x=size_tile_x, y=size_tile_y)
        return size_tile


def hex_to_rgb(hex_code, hsl=False):
    """Converts a HEX code into RGB or HSL.

    Based on an accepted answer at StackOverflow:
    https://stackoverflow.com/questions/29643352/

    Parameters
    ----------
    hex_code : str
        HEX code of 3 (short) or 6 (long) characters.
        Examples: '#FFF', '#FF0000'.
    hsl : bool, optional
        Whether to converts the given HEX code into HSL value.
        The default value is False.

    Returns
    -------
    List of length 3 consisting of either int or float
    RGB (or HSL) values.

    Raises
    ------
    ValueError: If input is not a valid HEX code.

    """
    pattern = r'#[a-fA-F0-9]{3}(?:[a-fA-F0-9]{3})?$'
    if not re.compile(pattern).match(hex_code):
        raise ValueError(f'"{hex_code}" is not a valid HEX code.')

    div = 255.0 if hsl else 0
    if len(hex_code) <= 4:
        return list(int(hex_code[i]*2, 16) / div if div else
                    int(hex_code[i]*2, 16) for i in (1, 2, 3))
    return list(int(hex_code[i:i+2], 16) / div if div else
                int(hex_code[i:i+2], 16) for i in (1, 3, 5))
