"""Setup the package."""
from setuptools import setup, find_packages
import re
VERSIONFILE = "src/OGCImagePrep/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError(f"Unable to find version string in {VERSIONFILE}.")

setup(
    name="OGCImagePrep",
    version=verstr,
    packages=find_packages(where="src"),
    package_dir={"": "src"},
)
