# OGCImagePrep 
## Overview
This package is meant to help obtain images from OGC data formats. More information on OGC can be found here https://en.wikipedia.org/wiki/Open_Geospatial_Consortium. Examples of OGC standards used in Waterschappen are Web Map Services and Web Map Tile Services. Currently only retrieval from Web Map Tile Services are implemented. 
## Installation
Due to the use of some c based python libraries it is recommended to create a new conda environment using the environment.yml file.
You can create this new environment by running the following:
```sh
conda env create --name my_env -f https://gitlab.com/hetwaterschapshuis/kenniscentrum/information-retrieval/OGCImagePrep/-/raw/main/environment.yml
```
Then you can install the OGCImagePrep by running:

```sh
conda activate my_env
pip install git+https://gitlab.com/hetwaterschapshuis/kenniscentrum/information-retrieval/OGCImagePrep
```

## Usage
See example.ipynb or the short description underneath.

To download images from a WMTS (Web Map Tile Service) you first need to initiate the WMTSConnector class. This class requires the link of the WMTS, the layer and tile matrix set you want to access images from, from what zoom level you want to obtain images and lastly in what format these pictures are.
```sh
wmts_link = "https://tiles.arcgis.com/tiles/BZiPrSbS4NknjGsQ/arcgis/rest/services/Luchtfoto_2020/MapServer/WMTS"
layer = "Luchtfoto_2020"
tile_matrix_set = "default028mm"
zoom_level=15
tile_format = "image/jpg"

wc = WMTSConnector(wmts_link, layer, tile_matrix_set, zoom_level, tile_format)
```
There are 2 methods to retrieve images from this class. The first one using the get_tile_at_coords which only requires a x and y coordinate and returns the tile on which this coordinate is located.
```sh
img = wc.get_tile_at_coords(x=194200.7371, y=539161.7876)
```
The coordinate can be at any random place on this tile. Since often you would like to obtain an image centered around a x and y coordinate there is also the get_image_around_coords. This method is able to obtain multiple tiles from the WMTS and bundle them together to create a picture of your required width and length. The following code would obtain a picture representing 50 meter x 50 meter.
```sh
img = wc.get_image_around_coords(x=194200.7371, y=539161.7876, width_x=50, length_y=50)
```

Images are returned as numpy array's and can be plotted using the matplotlib package.

## Future plans
Future plans might involve implementing other OGC data format, further adding functionality to allow obtaining images in YOLO format and/or adding translation of coordinate reference systems.